$('.main-carousel').flickity({
    // options
    cellAlign: 'center',
    contain: true,
    pageDots: false,
    wrapAround: true
});


window.onload = () => {

    let spoilers = document.querySelectorAll(".spoiler");
    spoilers.forEach(spoiler => {
        spoiler.addEventListener("click", () => {
            if(spoiler.classList.contains("active")){
                spoiler.classList.remove("active");
                return
            }
            spoilers.forEach(el => {
                if (el.classList.contains("active")) {
                    el.classList.remove("active");
                }
            })
            spoiler.classList.add("active")
        })
    });


    let menu_button = document.querySelector("#toggle-mobile-menu")
    let main_wrapper = document.querySelector("#main-wrapper")

    menu_button.addEventListener("click", () => {
        main_wrapper.classList.toggle('menu-open')
    })
}